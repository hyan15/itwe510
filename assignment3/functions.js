function filter(c, arr) {
	var r = [];
	arr.forEach(function(e, idx) {
		if (c(e, idx)) {
			r.push(e);
		}
	});

	return r;
}

function multiplyAll(arr) {
	return function(n) {
		return arr.map(function(e) { return n * e; });
	};
}

function buildFun(n) {
    var res = [];
    for (var i = 0; i < n; i++) {
    	(function(j) {
    		res.push(function() { return j; });
    	})(i);
    }
    return res;
}

function buildFun2(n) {
    var res = [];
    for (let i = 0; i < n; i++) {
        res.push(function() { return i; });
    }
    return res;
}

function once(fn) {
	return function() {
		var r;
		if (fn) {
			r = fn.apply(this, arguments);
			fn = null;
		}

		return r;
	}
}

console.log(filter(function(el, index) {
    return el % 2 == 0;
}, [1, 2, 3, 4, 5, 6]));
console.log(filter(function(el, index) {
    return el.length > 3;
}, ['a', 'ab', 'abc', 'abcd', 'abcde', 'abcdef']));

console.log(multiplyAll([1, 2, 3])(2));

buildFun(5).forEach(function(c) { console.log(c()); });
buildFun2(5).forEach(function(c) { console.log(c()); });

function add(a, b){
    return a + b;
}
var addOnce = once(add);
console.log(addOnce(5, 6));
console.log(addOnce(1, 3));
