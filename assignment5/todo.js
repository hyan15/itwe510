class Storage {
	constructor() {
		this._items = [];
	}

	find(query, callback) {
		callback = callback || function() {};

		callback.call(this, this._items.filter(function(item) {
			for (var q in query) {
				if (query[q] !== item[q]) {
					return false;
				}
			}
			return true;
		}));
	}

	findAll(callback) {
		callback = callback || function() {};

		this.find({}, callback);
	}

	save(updateData, callback, id) {
		callback = callback || function() {};

		if (id) {
			var updatedItem;
			for (var i = 0; i < this._items.length; i++) {
				if (this._items[i].id === id) {
					updatedItem = this._items[i];
					for (var k in updateData) {
						this._items[i][k] = updateData[k];
					}
					break;
				}
			}
			callback.call(this, updatedItem);

			return updatedItem;
		} else {
			updateData.id = new Date().getTime();
			this._items.push(updateData);
			callback.call(this, updateData);

			return updateData;
		}
	}

	remove(id, callback) {
		callback = callback || function() {};

		var deleted_items;

		for (var i = 0; i < this._items.length; i++) {
			if (this._items[i].id === id) {
				deleted_items = this._items.splice(i, 1);
				break;
			}
		}
		callback.call(this, deleted_items);
	}

	removeAll(callback) {
		callback = callback || function() {};
		var items = this._items.splice(0);
		callback(items);
	}
}

class Todo {
	constructor(storage) {
		this.storage = storage;
	}

	create(title, completed, callback) {
		title = title || '';
		completed = !!completed || false;
		callback = callback || function() {};

		this.storage.save({title: title, completed: completed}, callback);
	}

	find(query, callback) {
		var queryType = typeof query;

		if (queryType === 'function') {
			callback = query;
			this.storage.findAll(callback);
		} else if (queryType === 'string' || queryType === 'number') {
			query = parseInt(query, 10);
			this.storage.find({id: query}, callback);
		} else {
			this.storage.find(query, callback);
		}
	}

	update(id, data, callback) {
		this.storage.save(data, callback, id);
	}

	delete(id, callback) {
		this.storage.remove(id, callback);
	}

	deleteAll(callback) {
		this.storage.removeAll(callback);
	}
}

class TodoView {
	constructor(todo) {
		this.todo = todo;

		this.template = '<li data-id="{{id}}" class="todo-item {{completed}}">';
		this.template += '<div class="todo-item-wrapper">'
		this.template += '<input class="toggle" type="checkbox" name="todo-toggler" {{checked}} />'
		this.template += '<label>{{title}}</label>'
		this.template += '</div>'
		this.template += '</li>'
	}

	render() {
		var data = {
			id: this.todo.id,
			title: this.todo.title,
			completed: this.todo.completed ? 'completed' : '',
			checked: this.todo.completed ? 'checked' : ''
		};

		var html = this.template;
		['id', 'title', 'completed', 'checked'].forEach(function(k) {
			html = html.replace('{{' + k + '}}', data[k]);
		});

		var div = document.createElement('div');
		div.innerHTML = html;

		return div.firstChild;
	}
}

class TodoList {
	constructor() {
		this.todoList = document.querySelector('.todo-list');
		this.todoTitleInput = document.querySelector('input[name="todo-title"]');
  		this.addTodoButton = document.querySelector('#add-todo');
	}

	addTodo(todo) {
		this.todoList.appendChild(new TodoView(todo).render());
	}

	updateTodo(todo) {
		var li = document.querySelector('[data-id="' + todo.id + '"]');
		if (!li) {
			return;
		}

		li.className = todo.completed ? 'completed' : '';
		li.querySelector('input').checked = todo.completed;
	}

	bind(event, handler) {
		if (event == 'newTodo') {
		  	this.addTodoButton.addEventListener('click', function() {
		  		var todo = {
		  			title: this.todoTitleInput.value,
		  			completed: false
		  		};
		  		handler(todo);
		  		this.todoTitleInput.value = '';
		  	}.bind(this));
		} else if (event == 'itemToggle') {
		  	this.todoList.addEventListener('click', function(e) {
		  		var targetElement = e.target;
		  		var li = parent(targetElement, 'li');
		  		if (!li) {
		  			return;
		  		}

		  		var id = parseInt(li.dataset.id, 10);
		  		var completed = e.target.checked;
		  		handler({
		  			id: id,
		  			completed: completed
		  		});
		  	}.bind(this));
		}
	}
}

window.parent = function (element, tagName) {
	if (!element.parentNode) {
		return;
	}
	if (element.parentNode.tagName.toLowerCase() === tagName.toLowerCase()) {
		return element.parentNode;
	}
	return window.parent(element.parentNode, tagName);
};
