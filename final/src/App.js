import React, { Component } from 'react';
import { Layout } from 'antd';

import BibleApi from './bible_api';

import BibleMenu from './bible_menu';
import BibleReader from './bible_reader';

import './App.css';

const { Header, Sider, Content } = Layout;


class App extends Component {
  constructor(props) {
    super(props)

    this.state = {
      bibleGroups: [],
      selectedBibleGroup: null,
      selectedBook: null,
      bibleVersion: '和合本圣经（简体中文）'
    };
  }

  componentDidMount() {
    BibleApi.getBooks().then(bibleGroups => {
      var selectedBibleGroup = this.state.selectedBibleGroup;
      if (selectedBibleGroup === null) {
        selectedBibleGroup = bibleGroups[0];
      }

      var selectedBook = this.state.selectedBook;
      if (selectedBook == null) {
        selectedBook = selectedBibleGroup.books[0];
      }

      this.setState({
        bibleGroups: bibleGroups,
        selectedBibleGroup: selectedBibleGroup,
        selectedBook: selectedBook,
        bibleVersion: this.state.bibleVersion
      });
    });
  }

  onBookChange(item) {
    console.log(`onBookChange(${item.key})`);
    var bookKey = item.key;
    if (this.state.selectedBook && this.state.selectedBook.sn === bookKey) {
      console.log('Book does not change!');
      return;
    }

    var newBook = null;
    var newBibleGroup = this.state.bibleGroups.find(function(bibleGroup) {
      newBook = bibleGroup.books.find(function(book) {
        return book.abbreviation === bookKey;
      });

      return newBook !== undefined;
    });
    if (newBibleGroup && newBook) {
      this.setState({
        bibleGroups: this.state.bibleGroups,
        selectedBibleGroup: newBibleGroup,
        selectedBook: newBook,
        bibleVersion: this.state.bibleVersion
      });
    }
  }

  render() {
    return (
      <div className="app">
        <Layout>
          <Sider theme="light" collapsible collaspsed='false'>
            <BibleMenu
              bibleGroups={this.state.bibleGroups}
              selectedBibleGroup={this.state.selectedBibleGroup}
              selectedBook={this.state.selectedBook}
              onBookChange={this.onBookChange.bind(this)}
            />
          </Sider>
          <BibleReader
            version={this.state.bibleVersion}
            book={this.state.selectedBook}
            chapter={null}
          />
        </Layout>
      </div>
    );
  }
}

export default App;
