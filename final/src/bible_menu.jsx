import React, { Component } from 'react';
import { Menu } from 'antd';


class BibleMenu extends Component {
  render() {
    const bibleGroups = this.props.bibleGroups.slice();
    console.log(bibleGroups);
    var groupMenus = bibleGroups.map(function(bibleGroup) {
      var menuItems = bibleGroup.books.map(function(book) {
        return (
          <Menu.Item
            key={book.abbreviation}
            onClick={this.props.onBookChange}
          >
            {book.fullName}
          </Menu.Item>
        );
      }.bind(this));

      return (
        <Menu.SubMenu
          key={bibleGroup.key}
          title={bibleGroup.name}
        >
          {menuItems}
        </Menu.SubMenu>
      );
    }.bind(this));

    return (
      <Menu
        mode="inline"
        forceSubMenuRender={true}
        selectedKeys={(this.props.selectedBook ? [this.props.selectedBook.abbreviation] : [])}
        openKeys={this.props.bibleGroups.map(bibleGroup => bibleGroup.key)}
      >
        {groupMenus}
      </Menu>
    );
  }
}

export default BibleMenu;
