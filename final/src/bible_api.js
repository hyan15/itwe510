class BibleApi {
  static getBooks() {
    return fetch('https://immense-dawn-54030.herokuapp.com/bible')
      .then(response => response.json())
      .then(resp => {
        var oldTestament = {
          key: 'Old',
          name: '旧约',
          books: []
        };
        var newTestament = {
          key: 'New',
          name: '新约',
          books: []
        };
        resp.forEach(function(book) {
          var formatedBook = {
            sn: book.SN,
            chapterNumber: book.ChapterNumber,
            abbreviation: book.PinYin,
            fullName: book.FullName,
            shortName: book.ShortName
          };
          if (book.NewOrOld === 0) {
            oldTestament.books.push(formatedBook);
          } else {
            newTestament.books.push(formatedBook);
          }
        });

        return [oldTestament, newTestament];
      }).catch(error => {
        return error;
      });
  }

  static getChapter(bookSn, chapterSn) {
    var chapter = {
      bookSn: bookSn,
      chapterSn: chapterSn,
      verses: []
    };

    return fetch(`https://immense-dawn-54030.herokuapp.com/bible/${bookSn}/${chapterSn}`)
      .then(response => response.json())
      .then(resp => {
        resp.forEach(function(verse) {
          var formatedVerse = {
            verseSn: verse.VerseSN,
            lection: verse.Lection
          };
          chapter.verses.push(formatedVerse);
        });

        return chapter;
      }).catch(error => {
        return error;
      });
  }
}

export default BibleApi;
