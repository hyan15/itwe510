import React, { Component } from 'react';
import { Layout, Icon, List, Pagination } from 'antd';

import BibleApi from './bible_api';

const { Header, Content } = Layout;

class BibleVersion extends Component {
  render() {
    return (
      <h3 style={{float: 'left'}}>{this.props.version}</h3>
    );
  }
}


class BookIndicator extends Component {
  render() {
    return (
      <h3 style={{textAlign: 'center'}}>{this.props.bookName} {this.props.chapterSn}章</h3>
    );
  }
}


class BookMenu extends Component {
  chapterRender(current, type, originalElement) {
    if (type === 'prev' || type === 'next') {
      return;
    } else {
      return originalElement;
    }
  }

  render() {
    return (
      <Pagination
        style={{margin: '1em 0'}}
        pageSize={this.props.chapterSize}
        current={this.props.chapterSn}
        total={this.props.chapterNumber}
        itemRender={this.chapterRender.bind(this)}
        onChange={this.props.onChapterChange}
      />
    );
  }
}


class ChapterVerse extends Component {
  render() {
    const verses = this.props.verses.slice();

    return (
      <List
        bordered
        split={true}
        dataSource={verses}
        renderItem={verse => (<List.Item>{verse.verseSn} {verse.lection}</List.Item>)}
      />
    );
  }
}


class BibleReader extends Component {
  constructor(props) {
    super(props);

    this.state = {
      currentChapter: props.chapter
    };
  }

  componentDidUpdate(prevProps) {
    if (this.props.book == null) {
      return;
    }

    if (prevProps.book != null && this.props.book.sn === prevProps.book.sn) {
      return;
    }

    var bookSn = this.props.book.sn;
    var chapterSn = 1;
    this.loadChapter(bookSn, chapterSn);
  }

  onChapterChange(chapterSn) {
    var bookSn = this.props.book.sn;
    if (chapterSn != this.state.currentChapter.chapterSn) {
      this.loadChapter(bookSn, chapterSn);
    }
  }

  loadChapter(bookSn, chapterSn) {
    BibleApi.getChapter(bookSn, chapterSn).then(chapter => {
      this.setState({currentChapter: chapter})
    });
  }

  render() {
    return (
      <Layout>
        <Header style={{ background: '#fff', padding: 0 }}>
          <Icon className="trigger" type="menu-fold" style={{
            float: 'left', margin: '1.75em 1em'
          }} />
          <BibleVersion version={this.props.version} />
        </Header>
        <Content style={{
          margin: '24px 16px', padding: 24, background: '#fff', minHeight: 280,
        }}
        >
          <BookIndicator
            bookName={this.props.book ? this.props.book.fullName : ''}
            chapterSn={this.state.currentChapter ? this.state.currentChapter.chapterSn : 1}
          />
          <BookMenu
            chapterSize={1}
            chapterNumber={this.props.book ? this.props.book.chapterNumber : 1}
            chapterSn={this.state.currentChapter ? this.state.currentChapter.chapterSn : 1}
            onChapterChange={this.onChapterChange.bind(this)}
          />
          <ChapterVerse
            verses={this.state.currentChapter ? this.state.currentChapter.verses : []}
          />
        </Content>
      </Layout>
    );
  }
}

export default BibleReader;
