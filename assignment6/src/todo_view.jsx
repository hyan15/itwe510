import React from 'react';

class TodoView extends React.Component {
  render() {
    return (
      <li className={"todo-item " + (this.props.todo.completed ? 'completed' : '')}>
        <div className="todo-item-wrapper">
          <input
            className="toggle"
            type="checkbox"
            name="todo-toggler"
            checked={this.props.todo.completed}
            onChange={() => this.props.onToggle()}
          />
          <label>{this.props.todo.title}</label>
        </div>
      </li>
    );
  }
}

export default TodoView;
