import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

import TodoList from './todo_list';

ReactDOM.render(
  <TodoList />,
  document.getElementById('root')
);
