import React from 'react';

import TodoView from './todo_view';

class TodoList extends React.Component {
  constructor(props) {
    super(props)

    this.titleInput = React.createRef();

    this.state = {
      todos: []
    };
  }

  addTodo() {
    var title = this.titleInput.current.value;
    this.titleInput.current.value = '';

    const todos = this.state.todos.slice();
    todos.push({
      id: new Date().getTime(),
      title: title,
      completed: false
    });
    this.setState({todos: todos});
  }

  toggleTodo(todo) {
    const todos = this.state.todos.slice();
    var idx = todos.findIndex(function(t) {
      return t.id == todo.id;
    });
    todos[idx] = Object.assign({}, todos[idx], {completed: !todos[idx].completed});
    this.setState({todos: todos})
  }

  render() {
    const todos = this.state.todos.slice();

    var todoViews = todos.map(function(todo) {
      return (
        <TodoView key={todo.id} todo={todo} onToggle={this.toggleTodo.bind(this, todo)} />
      );
    }.bind(this));

    return (
      <div>
        <h1>My ToDo List</h1>
        <div className="todo-list-container">
          <ul className="todo-list">
            {todoViews}
          </ul>
        </div>
        <div className="todo-form">
          <input type="text" ref={this.titleInput} />
          <button type="submit" onClick={() => this.addTodo()}>Submit</button>
        </div>
      </div>
    );
  }
}

export default TodoList;
