const bodyParser = require('body-parser');

module.exports = {
  toDosHandler: function(app, server){
    var todos = [
      {
        id: 1,
        content: 'Play Games',
        done: true
      },{
        id: 2,
        content: 'Finish Frontend Web Development Homework',
        done: false
      }
    ]; // {id: [array index], content: [to do content]}
    var idTracker = 3;

    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: false }));

    app.get('/to-do-lists', (req, res) => {
      res.json({ todos:  todos});
    });

    app.post('/to-do-lists', (req, res) => {
      const content = req.body.todoItem;
      const todo = { id: idTracker, content: content, done: false };
      todos.push(todo);
      idTracker++;
      res.json({todo: todo});
    });

    app.put('/to-do-lists/:id/done', (req, res) => {
      const todo = updateToDoStatus(req, true);
      if (todo){
        res.json({todo: todo});
      }else{
        res.sendStatus(404);
      }
      
    });

    app.put('/to-do-lists/:id/notdone', (req, res) => {
      const todo = updateToDoStatus(req, false);
      if (todo){
        res.json({todo: todo});
      }else{
        res.sendStatus(404);
      }
    });

    app.delete('/to-do-lists/:id', (req, res) => {
      const index = getIndexById(req.params.id);
      if (index != -1) {
        todos.splice(index, 1);
        res.sendStatus(200);
      }else{
        res.sendStatus(404);
      }
    })

    function updateToDoStatus(req, done){
      const todoId = req.params.id;
      const todo = getToDoById(todoId);
      todo.done = done;
      return todo;
    }

    function getToDoById(id){
      return todos.find((e) => e.id == id);
    }

    function getIndexById(id){
      return todos.findIndex((e) => e.id == id)
    }
  }
}