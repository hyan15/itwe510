function parent(element, tagName) {
	if (!element.parentNode) {
		return;
	}
	if (element.parentNode.tagName.toLowerCase() === tagName.toLowerCase()) {
		return element.parentNode;
	}
	return parent(element.parentNode, tagName);
}

export { parent };
