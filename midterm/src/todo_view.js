class TodoView {
	constructor(todo) {
		this.todo = todo;

		this.template = '<li data-id="{{id}}" class="todo-item {{completed}}">';
		this.template += '<div class="todo-item-wrapper">'
		this.template += '<input class="toggle" type="checkbox" name="todo-toggler" {{checked}} />'
		this.template += '<label>{{title}}</label>'
		this.template += '</div>'
		this.template += '</li>'
	}

	render() {
		var data = {
			id: this.todo.id,
			title: this.todo.title,
			completed: this.todo.completed ? 'completed' : '',
			checked: this.todo.completed ? 'checked' : ''
		};

		var html = this.template;
		['id', 'title', 'completed', 'checked'].forEach(function(k) {
			html = html.replace('{{' + k + '}}', data[k]);
		});

		var div = document.createElement('div');
		div.innerHTML = html;

		return div.firstChild;
	}
}

export default TodoView;
