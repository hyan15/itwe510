class TodoModel {
	constructor(storage) {
		this.storage = storage;
	}

	create(title, completed, callback) {
		title = title || '';
		completed = !!completed || false;
		callback = callback || function() {};

		this.storage.save({title: title, completed: completed}, callback);
	}

	find(query, callback) {
		var queryType = typeof query;

		if (queryType === 'function') {
			callback = query;
			this.storage.findAll(callback);
		} else if (queryType === 'string' || queryType === 'number') {
			query = parseInt(query, 10);
			this.storage.find({id: query}, callback);
		} else {
			this.storage.find(query, callback);
		}
	}

	update(id, data, callback) {
		this.storage.save(data, callback, id);
	}

	delete(id, callback) {
		this.storage.remove(id, callback);
	}

	deleteAll(callback) {
		this.storage.removeAll(callback);
	}
}

export default TodoModel;
