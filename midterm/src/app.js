import Storage from './server_storage';
import TodoModel from './todo_model';
import TodoList from './todo_list';

import './style.scss'

window.addEventListener('load', function() {
	var storage = new Storage();
	storage.setBackend('http://localhost:8009');

	var todoModel = new TodoModel(storage);
	var todoList = new TodoList('#to-do-list', 'textarea[name="todo-item"]', 'button[type="submit"]');
	todoModel.find(function(todos) {
		todos.forEach(function(todo) {
			todoList.addTodo(todo);
		});
	});

	todoList.bind('newTodo', function(data) {
		todoModel.create(data.title, data.completed, todoList.addTodo.bind(todoList));
	});
	todoList.bind('itemToggle', function(data) {
		var id = data.id;
		delete data.id;
		todoModel.update(id, data, todoList.updateTodo.bind(todoList));
	});
});
