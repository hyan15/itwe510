import { parent } from './utils';
import TodoView from './todo_view';


class TodoList {
	constructor(todoContainerSelector, todoTitleSelector, todoSubmitSelector) {
		this.todoList = document.querySelector(todoContainerSelector);
		this.todoTitleInput = document.querySelector(todoTitleSelector);
  		this.addTodoButton = document.querySelector(todoSubmitSelector);
	}

	addTodo(todo) {
		this.todoList.appendChild(new TodoView(todo).render());
	}

	updateTodo(todo) {
		var li = document.querySelector('[data-id="' + todo.id + '"]');
		if (!li) {
			return;
		}

		li.className = todo.completed ? 'completed' : '';
		li.querySelector('input').checked = todo.completed;
	}

	bind(event, handler) {
		if (event == 'newTodo') {
		  	this.addTodoButton.addEventListener('click', function(e) {
		  		e.preventDefault();
		  		e.stopPropagation();
		  		var todo = {
		  			title: this.todoTitleInput.value,
		  			completed: false
		  		};
		  		handler(todo);
		  		this.todoTitleInput.value = '';
		  	}.bind(this));
		} else if (event == 'itemToggle') {
		  	this.todoList.addEventListener('click', function(e) {
		  		var targetElement = e.target;
		  		var li = parent(targetElement, 'li');
		  		if (!li) {
		  			return;
		  		}

		  		var id = parseInt(li.dataset.id, 10);
		  		var completed = e.target.checked;
		  		handler({
		  			id: id,
		  			completed: completed
		  		});
		  	}.bind(this));
		}
	}
}

export default TodoList;
