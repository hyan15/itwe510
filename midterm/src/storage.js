class Storage {
	constructor() {
		this._items = [];
	}

	find(query, callback) {
		callback = callback || function() {};

		callback.call(this, this._items.filter(function(item) {
			for (var q in query) {
				if (query[q] !== item[q]) {
					return false;
				}
			}
			return true;
		}));
	}

	findAll(callback) {
		callback = callback || function() {};

		this.find({}, callback);
	}

	save(updateData, callback, id) {
		callback = callback || function() {};

		if (id) {
			var updatedItem;
			for (var i = 0; i < this._items.length; i++) {
				if (this._items[i].id === id) {
					updatedItem = this._items[i];
					for (var k in updateData) {
						this._items[i][k] = updateData[k];
					}
					break;
				}
			}
			callback.call(this, updatedItem);

			return updatedItem;
		} else {
			updateData.id = new Date().getTime();
			this._items.push(updateData);
			callback.call(this, updateData);

			return updateData;
		}
	}

	remove(id, callback) {
		callback = callback || function() {};

		var deleted_items;

		for (var i = 0; i < this._items.length; i++) {
			if (this._items[i].id === id) {
				deleted_items = this._items.splice(i, 1);
				break;
			}
		}
		callback.call(this, deleted_items);
	}

	removeAll(callback) {
		callback = callback || function() {};
		var items = this._items.splice(0);
		callback(items);
	}
}

export default Storage;
