class ServerStorage {
	setBackend(backend) {
		this.backend = backend;
	}

	find(query, callback) {
		callback = callback || function() {};

		var xhr = new XMLHttpRequest();
		xhr.onload = function(e) {
			var resp = JSON.parse(xhr.responseText);
			var todos = resp.todos.map(function(t) {
				return {
					id: t.id,
					title: t.content,
					completed: t.done
				};
			});
			callback.call(this, todos.filter(function(item) {
				for (var q in query) {
					if (query[q] !== item[q]) {
						return false;
					}
				}
				return true;
			}));
		}.bind(this);

		xhr.open('GET', this.backend + '/to-do-lists');
		xhr.send();
	}

	findAll(callback) {
		callback = callback || function() {};

		this.find({}, callback);
	}

	save(updateData, callback, id) {
		callback = callback || function() {};

		var xhr = new XMLHttpRequest();
		var method = 'POST';
		var body = null;
		var url = this.backend + '/to-do-lists';

		xhr.onload = function() {
			var resp = JSON.parse(xhr.responseText);
			var todo = {
				id: resp.todo.id,
				title: resp.todo.content,
				completed: resp.todo.done
			};

			callback.call(this, todo);
		};

		if (id) {
			method = 'PUT';
			if (updateData.completed) {
				url = this.backend + '/to-do-lists/' + id + '/done';
			} else {
				url = this.backend + '/to-do-lists/' + id + '/notdone';
			}
		} else {
			var todoData = {
				todoItem: updateData.title
			};
			body = JSON.stringify(todoData);
		}

		xhr.open(method, url);
		xhr.setRequestHeader('Content-Type', 'application/json');
		xhr.send(body);
	}

	remove(id, callback) {
		callback = callback || function() {};

		var xhr = new XMLHttpRequest();
		xhr.open('DELETE', this.backend + '/to-do-lists/' + id)
		xhr.send();
	}

	removeAll(callback) {
	}
}

export default ServerStorage;
