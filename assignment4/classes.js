class Queue {
	constructor() {
		this._q = [];
		this._length = 0;
		this._cursor = 0;
	}

	enqueue(e) {
		this._q.push(e);
		this._length += 1;
	}

	dequeue() {
		if (this.isEmpty()) {
			throw 'Queue is empty!'
		}

		var e = this._q.shift();
		this._length -= 1;

		return e;
	}

	length() {
		return this._length;
	}

	isEmpty() {
		return this._length <= 0;
	}

	[Symbol.iterator]() {
		return {
			next: () => {
				if (this._cursor < this._q.length) {
					return {value: this._q[this._cursor++], done: false}
				} else {
					this._cursor = 0;
					return {done: true};
				}

			}
		};
	}
}

var queue = new Queue();
queue.enqueue(1);
queue.enqueue(3.14);
queue.enqueue('a');
console.log(queue);

for (let e of queue) {
	console.log(e);
}

console.log('Length: ' + queue.length());
console.log('Dequeue: ' + queue.dequeue());
console.log('Length: ' + queue.length());
console.log('isEmpty: ' + queue.isEmpty());
console.log('Dequeue: ' + queue.dequeue());
console.log('Dequeue: ' + queue.dequeue());
console.log('isEmpty: ' + queue.isEmpty());
