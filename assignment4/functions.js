function Queue() {
	this._q = [];
	this._length = 0;
}

Queue.prototype.enqueue = function(e) {
	this._q.push(e);
	this._length += 1;
};

Queue.prototype.dequeue = function() {
	if (this.isEmpty()) {
		throw 'Queue is empty!'
	}

	var e = this._q.shift();
	this._length -= 1;

	return e;
};

Queue.prototype.length = function() {
	return this._length;
};

Queue.prototype.isEmpty = function() {
	return this._length <= 0;
};

Queue.prototype.forEach = function(c) {
	this._q.forEach(c);
};

var queue = new Queue();
queue.enqueue(1);
queue.enqueue(3.14);
queue.enqueue('a');
console.log(queue);

queue.forEach(function(ele) {
	console.log(ele);
});

console.log('Length: ' + queue.length());
console.log('Dequeue: ' + queue.dequeue());
console.log('Length: ' + queue.length());
console.log('isEmpty: ' + queue.isEmpty());
console.log('Dequeue: ' + queue.dequeue());
console.log('Dequeue: ' + queue.dequeue());
console.log('isEmpty: ' + queue.isEmpty());
