function factorial(n) {
	if (n <= 0)
		return 1;

	for (var i = 1, r = 1; i <= n; i++) {
		r = i * r;
	}

	return r;
}

function ext(filename) {
	return filename.slice(filename.lastIndexOf('.') + 1);
}

function isLeapYear(year) {
	return year % 4 == 0 && (year % 100 != 0 || year % 400 == 0);
}

function evenDoubleSum(arr) {
	return arr.filter(function(n) { return n % 2 == 0; }).
		map(function(n) { return n * 2; }).
		reduce(function(sum, n) { return sum + n; }, 0);
}

function isEmail(str) {
	return (/^[0-9a-zA-Z]+@[0-9a-zA-Z]+\.[0-9a-zA-Z]+$/i).test(str);
}

console.log(factorial(4));

console.log(ext('abc.txt'));
console.log(ext('abc.tar.gz'));
console.log(ext('index.html.erb'));

console.log(isLeapYear(1996));
console.log(isLeapYear(1997));
console.log(isLeapYear(1900));
console.log(isLeapYear(2000));

console.log(evenDoubleSum([1,2,3,4,5,6,7,8,9]));
console.log(evenDoubleSum([1,3,5,7,9]));
console.log(evenDoubleSum([2, 4]));

console.log(isEmail('123'));
console.log(isEmail('@abc.com'));
console.log(isEmail('abc@123'));
console.log(isEmail('abc@gmail.com'));
console.log(isEmail('123456@qq.com'));
